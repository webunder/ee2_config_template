<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'expressionengine';
$active_record = TRUE;

switch ( $_SERVER['HTTP_HOST'] ) {

    // local
    case 'local_server_host' :                                      // localhost, projectname.dev
    $db['expressionengine']['hostname'] = 'local_server_host';
    $db['expressionengine']['username'] = 'local_db_user';
    $db['expressionengine']['password'] = 'local_db_password';
    $db['expressionengine']['database'] = 'local_db_name';
    break;

    // staging
    case 'staging_server_host' :                                    // staging.domain.com
    $db['expressionengine']['hostname'] = 'staging_server_host';
    $db['expressionengine']['username'] = 'staging_db_user';
    $db['expressionengine']['password'] = 'staging_db_password';
    $db['expressionengine']['database'] = 'staging_db_name';
    break;

    // live
    case 'live_server_host' :                                       // domain.com
    $db['expressionengine']['hostname'] = 'live_db_server';
    $db['expressionengine']['username'] = 'live_db_user';
    $db['expressionengine']['password'] = 'live_db_password';
    $db['expressionengine']['database'] = 'live_db_name';
    break;

}

$db['expressionengine']['dbdriver'] = 'mysql';
$db['expressionengine']['pconnect'] = FALSE;
$db['expressionengine']['dbprefix'] = 'exp_';
$db['expressionengine']['swap_pre'] = 'exp_';
$db['expressionengine']['db_debug'] = TRUE;
$db['expressionengine']['cache_on'] = FALSE;
$db['expressionengine']['autoinit'] = FALSE;
$db['expressionengine']['char_set'] = 'utf8';
$db['expressionengine']['dbcollat'] = 'utf8_general_ci';
$db['expressionengine']['cachedir'] = APPPATH."cache/db_cache/";





/* End of file database.php */
/* Location: ./system/expressionengine/config/database.php */
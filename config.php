<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| ExpressionEngine Config Items
|--------------------------------------------------------------------------
|
| The following items are for use with ExpressionEngine.  The rest of
| the config items are for use with CodeIgniter, some of which are not
| observed by ExpressionEngine, e.g. 'permitted_uri_chars'
|
*/


// SERVER FOLDER STRUCTURE
//
// -- private
// ---- updater
// ---- backup_pro
// ------ database
// ------ files
// -- public_html
// -- system

// GENERAL SETTINGS
$config['app_version']                      = '273';
$config['install_lock']                     = '';
$config['license_number']                   = '0000-0000-0000-0000';
$config['debug']                            = '1';
$config['system_folder']                    = 'system';
$config['is_site_on']                       = 'y';
$config['is_system_on']                     = 'y';
$config['allow_extensions']                 = 'y';
$config['site_label']                       = 'EE Config Template';
$config['cookie_prefix']                    = '';
$config['doc_url']                          = '/user_guide/';
$config['webmaster_email']                  = 'webmaster@' . $_SERVER['HTTP_HOST'];
$config['encryption_key']                   = 'TXbLQVbPQOvwV2uixpJRhgeMXbU7yvlrJgW9YcusoB0jTuAInpo2uCQQ';

// DEFAULT LOCATIONS
$base_url                                   = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http');
$base_url                                  .= '://'.$_SERVER['HTTP_HOST'];
$config['site_index']                       = '';
$config['site_url']                         = $base_url;
$config['server_path']                      = FCPATH;
$config['cp_url']                           = $base_url . '/admin.php';

// THEME PREFERENCES
$config['theme_folder_url']                 = $base_url .'/themes/';
$config['theme_folder_path']                = $config['server_path'] .'themes/';
$config['cp_theme']                         = 'default';

// TEMPLATE PREFERENCES
$config['save_tmpl_files']                  = 'y';
$config['save_tmpl_revisions']              = 'y';
$config['tmpl_file_basepath']               = APPPATH.'templates/';
$config['strict_urls']                      = 'y';
$config['site_404']                         = 'site/404';

// CODEIGNITER CONFIG ITEMS
$config['admin_session_type']        = 'c';
$config['cp_session_type']           = 'c';  // Changed in version 2.8: Variable was changed from admin_session_type to cp_session_type.
$config['auto_assign_cat_parents']   = 'y';
$config['auto_convert_high_ascii']   = 'y';
$config['autosave_interval_seconds'] = '0';
$config['ban_action']                = 'restrict';
$config['ban_destination']           = 'http://www.yahoo.com/';
$config['ban_message']               = 'This site is currently unavailable';
$config['banish_masked_ips']         = 'y';
$config['banishment_message']        = 'You have exceeded the allowed page load frequency.';
$config['banishment_type']           = 'message';
$config['banishment_url']            = '';
$config['banned_emails']             = '';
$config['banned_ips']                = '';
$config['banned_screen_names']       = '';
$config['banned_usernames']          = '';

// MEMBER PREFERENCES
$config['allow_member_registration'] = 'n';
$config['allow_username_change']     = 'n';
$config['profile_trigger']           = ' ' . rand(0,time()); // randomise the member profile trigger word
$config['allow_dictionary_pw']       = 'n';
$config['allow_member_localization'] = 'n';
$config['allow_multi_logins']        = 'y';
$config['allow_pending_login']       = 'n';
$config['allow_avatar_uploads']      = 'n';
$config['enable_avatars']            = 'y';
$config['avatar_max_height']         = '100';
$config['avatar_max_kb']             = '50';
$config['avatar_max_width']          = '100';
$config['avatar_url']                = '/images/avatars/';
$config['avatar_path']               = $config['server_path'].'images/avatars/';
$config['enable_emoticons']          = 'n';
$config['emoticon_url']              = "/images/smileys/";
$config['photo_path']                = $config['server_path'].'images/member_photos/';
$config['allow_signatures']          = 'n';
$config['sig_allow_img_upload']      = 'n';
$config['sig_img_url']               = $base_url.'/images/signature_attachments/';
$config['sig_img_path']              = $config['server_path'].'images/signature_attachments/';
$config['prv_msg_upload_path']       = $config['server_path'].'images/pm_attachments/';
$config['captcha_url']               = $base_url.'/images/captchas/';
$config['captcha_path']              = $config['server_path'] . 'images/captchas/';
$config['pw_min_len']                = '8';
$config['un_min_len']                = '7';


// HIDDEN CONFIGS
$config['expire_session_on_browser_close']  = 'y'; // Users Session Length 600 = 10min
$config['hidden_template_indicator']        = '_';
$config['autosave_interval_seconds']        = '30';
$config['autosave_prune_hours']             = '4';
$config['publish_page_title_focus']         = 'y';
$config['xss_clean_member_group_exception'] = '1|5'; // Sets the member group IDs to exclude XSS cleaning on.
$config['new_version_check']                = 'n';
$config['use_category_name']                = 'y';
$config['reserved_category_word']           = 'category';
$config['word_separator']                   = 'dash'; // dash|underscore
// $config['server_offset']                 = ''; // Amount of minutes to offset the server time by
// $config['daylight_savings']              = ((bool) date('I')) ? 'y' : 'n'; // Autodetect DST
$config['allow_textarea_tabs']              = 'y';


// DEBUGGING AND PERFORMANCE
$config['show_profiler']                    = 'n';
$config['template_debugging']               = 'n';
$config['debug']                            = '1'; // 0: no PHP/SQL errors shown. 1: Errors shown to Super Admins. 2: Errors shown to everyone.
$config['enable_sql_caching']               = 'n';
$config['email_debug']                      = 'n';
$config['enable_db_caching']                = 'n';
$config['enable_search_log']                = 'n';
$config['enable_throttling']                = 'n';

// EMAIL OPTIONS
$config['mail_protocol']                    = 'smtp'; // Possible options are mail, sendmail, and smtp
$config['smtp_server']                      = '';
$config['smtp_port']                        = '';
$config['smtp_username']                    = '';
$config['smtp_password']                    = '';

// TRACKING OPTIONS
$config['disable_all_tracking']             = 'y';
$config['enable_online_user_tracking']      = 'n';
$config['dynamic_tracking_disabling']       = '500';
$config['enable_hit_tracking']              = 'n';
$config['enable_entry_view_tracking']       = 'n';
$config['log_referrers']                    = 'n';

// UPLOAD PREFERENCES
$config['filename_increment'] = 'y';
$config['upload_preferences'] = array (
	1 =>
	array (
		'name' => 'Main Uploads',
		'server_path' => FCPATH . 'library/uploads/',
		'url' => '/library/uploads/',
	),
);


// 3RD PARTY ADD-ON CONFIG ITEMS

// LOW VARIABLES PREFERENCES
$config['low_variables_save_as_files']      = 'y';
$config['low_variables_file_path']          = APPPATH.'variables/';

// CE IMAGE SETTINGS
$config['ce_image_cache_dir']               = '/library/cache/';
$config['ce_image_remote_dir']              = '"/library/cache/';
$config['ce_image_memory_limit']            = 64;
$config['ce_image_remote_cache_time']       = 1440;
$config['ce_image_quality']                 = 100;
$config['ce_image_disable_xss_check']       = 'yes';

// UPDATER PREFERENCES
$config['updater']['file_transfer_method']  = 'local'; // local, ftp, sftp

$config['updater']['ftp']['hostname']       = '';
$config['updater']['ftp']['username']       = '';
$config['updater']['ftp']['password']       = '';
$config['updater']['ftp']['port']           = '21';
$config['updater']['ftp']['passive']        = 'yes';
$config['updater']['ftp']['ssl']            = 'no';

$config['updater']['sftp']['hostname']      = '';
$config['updater']['sftp']['username']      = '';
$config['updater']['sftp']['password']      = '';
$config['updater']['sftp']['port']          = '22';

$config['updater']['path_map']['root']               = FCPATH; // Document Root
$config['updater']['path_map']['backup']             = str_replace('public_html', 'private/updater', FCPATH); // Backup Dir
$config['updater']['path_map']['system']             = str_replace('public_html', 'system', FCPATH); // System Dir
$config['updater']['path_map']['system_third_party'] = APPPATH . 'third_party/'; // Third Party dir system dir
$config['updater']['path_map']['themes']             = FCPATH . 'themes/'; // Themes dir
$config['updater']['path_map']['themes_third_party'] = FCPATH . 'themes/third_party/'; // Third Party dir in themes dir

// BACKUP PRO PREFERENCES
// $config['m62_backup']['exclude_paths']                  = array();
// $config['m62_backup']['allowed_access_levels']          = array();
// $config['m62_backup']['auto_threshold']                 = '0';
// $config['m62_backup']['backup_file_location']           = array(realpath($_SERVER['DOCUMENT_ROOT']), str_replace("public_html/", "system", FCPATH));
// $config['m62_backup']['backup_store_location']          = str_replace('public_html/', 'private/backup_pro', FCPATH);
// $config['m62_backup']['license_number']                 = '';
// $config['m62_backup']['db_backup_method']               = 'php';
// $config['m62_backup']['db_restore_method']              = 'php';
// $config['m62_backup']['date_format']                    = '%Y%m%d - %d %M %Y, %h:%i:%s:%A';
//
// $config['m62_backup']['cron_notify_emails']             = array('webmaster@domain.com');
// $config['m62_backup']['cron_attach_backups']            = '0';
// $config['m62_backup']['cron_attach_threshold']          = '0';
//
// $config['m62_backup']['ftp_hostname']                   = '';
// $config['m62_backup']['ftp_username']                   = '';
// $config['m62_backup']['ftp_password']                   = '0';
// $config['m62_backup']['ftp_port']                       = '21';
// $config['m62_backup']['ftp_passive']                    = '0';
// $config['m62_backup']['ftp_store_location']             = '';
//
// $config['m62_backup']['s3_access_key']                  = '';
// $config['m62_backup']['s3_secret_key']                  = '';
// $config['m62_backup']['s3_bucket']                      = '';
//
// $config['m62_backup']['cf_username']                    = '';
// $config['m62_backup']['cf_api']                         = '';
// $config['m62_backup']['cf_bucket']                      = '';
// $config['m62_backup']['cf_location']                    = 'us';

// LINK VAULT SEETINGS
// $config['link_vault_salt']               = 'mTjFsLfMDzPwbZvlIO4e';
// $config['link_vault_hidden_folder']      = '../private/';
// $config['link_vault_leech_url']          = 'https://www.google.com.au/';
// $config['link_vault_missing_url']        = 'https://www.google.com.au/';
// $config['link_vault_block_leeching']     = 0;
// $config['link_vault_log_leeching']       = 0;
// $config['link_vault_log_link_clicks']    = 0;
// $config['link_vault_debug']              = TRUE;
// $config['link_vault_aws_access_key']     = 'MYAMAZONWEBSERVICESACCESSKEY';
// $config['link_vault_aws_secret_key']     = 'thisismyamazonwebservicessecretkey';


// END EE config items

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|   http://example.com/
|
*/
$config['base_url'] = $config['site_url'];

/*
|--------------------------------------------------------------------------
| Index File
|--------------------------------------------------------------------------
|
| Typically this will be your index.php file, unless you've renamed it to
| something else. If you are using mod_rewrite to remove the page set this
| variable so that it is blank.
|
*/
$config['index_page'] = "";

/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of "AUTO" works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'            Default - auto detects
| 'PATH_INFO'       Uses the PATH_INFO
| 'QUERY_STRING'    Uses the QUERY_STRING
| 'REQUEST_URI'     Uses the REQUEST_URI
| 'ORIG_PATH_INFO'  Uses the ORIG_PATH_INFO
|
*/
$config['uri_protocol'] = 'AUTO';

/*
|--------------------------------------------------------------------------
| URL suffix
|--------------------------------------------------------------------------
|
| This option allows you to add a suffix to all URLs generated by CodeIgniter.
| For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/urls.html
*/

$config['url_suffix'] = '';

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language'] = 'english';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = 'UTF-8';

/*
|--------------------------------------------------------------------------
| Enable/Disable System Hooks
|--------------------------------------------------------------------------
|
| If you would like to use the "hooks" feature you must enable it by
| setting this variable to TRUE (boolean).  See the user guide for details.
|
*/
$config['enable_hooks'] = FALSE;


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = 'EE_';


/*
|--------------------------------------------------------------------------
| Allowed URL Characters
|--------------------------------------------------------------------------
|
| This lets you specify which characters are permitted within your URLs.
| When someone tries to submit a URL with disallowed characters they will
| get a warning message.
|
| As a security measure you are STRONGLY encouraged to restrict URLs to
| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
|
| Leave blank to allow all characters -- but only if you are insane.
|
| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
|
*/
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\\-';


/*
|--------------------------------------------------------------------------
| Enable Query Strings
|--------------------------------------------------------------------------
|
| By default CodeIgniter uses search-engine friendly segment based URLs:
| example.com/who/what/where/
|
| You can optionally enable standard query string based URLs:
| example.com?who=me&what=something&where=here
|
| Options are: TRUE or FALSE (boolean)
|
| The two other items let you set the query string "words" that will
| invoke your controllers and its functions:
| example.com/index.php?c=controller&m=function
|
| Please note that some of the helpers won't work as expected when
| this feature is enabled, since CodeIgniter is designed primarily to
| use segment based URLs.
|
*/
$config['enable_query_strings'] = FALSE;
$config['directory_trigger'] = 'D';
$config['controller_trigger'] = 'C';
$config['function_trigger'] = 'M';

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
|
|   0 = Disables logging, Error logging TURNED OFF
|   1 = Error Messages (including PHP errors)
|   2 = Debug Messages
|   3 = Informational Messages
|   4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = 0;

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/logs/ folder.  Use a full server path with trailing slash.
|
*/
$config['log_path'] = '';

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = 'Y-m-d H:i:s';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/cache/ folder.  Use a full server path with trailing slash.
|
*/
$config['cache_path'] = '';

/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Sessions class with encryption
| enabled you MUST set an encryption key.  See the user guide for info.
|
*/
$config['encryption_key'] = '';

/*
|--------------------------------------------------------------------------
| Global XSS Filtering
|--------------------------------------------------------------------------
|
| Determines whether the XSS filter is always active when GET, POST or
| COOKIE data is encountered
|
*/
$config['global_xss_filtering'] = FALSE;


/*
|--------------------------------------------------------------------------
| CSRF Protection
|--------------------------------------------------------------------------
|
| Determines whether Cross Site Request Forgery protection is enabled.
| For more info visit the security library page of the user guide
|
*/
$config['csrf_protection'] = FALSE;

/*
|--------------------------------------------------------------------------
| Output Compression
|--------------------------------------------------------------------------
|
| Enables Gzip output compression for faster page loads.  When enabled,
| the output class will test whether your server supports Gzip.
| Even if it does, however, not all browsers support compression
| so enable only if you are reasonably sure your visitors can handle it.
|
| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
| means you are prematurely outputting something to your browser. It could
| even be a line of whitespace at the end of one of your scripts.  For
| compression to work, nothing can be sent before the output buffer is called
| by the output class.  Do not "echo" any values with compression enabled.
|
*/
$config['compress_output'] = FALSE;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are "local" or "gmt".  This pref tells the system whether to use
| your server's local time as the master "now" reference, or convert it to
| GMT.  See the "date helper" page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = 'local';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = TRUE;


/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '';


/* End of file config.php */
/* Location: ./system/expressionengine/config/config.php */
